import Vue from 'vue';
import App from './App.vue';
import './plugins/axios';
import './plugins/element.js';
import router from './router';
import store from './store';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: function (h) {
    return h(App);
  }
}).$mount('#app');
