import login from '@/views/user/login';
import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        redirect: '/login'
    },
    {
        path: '/login',
        name: '登录页面',
        component: login
    },
    {
        path: '/register',
        name: '注册界面',
        component: () => import('@/views/user/register')
    },
    {
        path: '/forget',
        name: '忘记密码',
        component: () => import('@/views/user/forget')
    },
    {
        path: '/index',
        name: '主页',
        component: () => import('@/views/index'),
        children: [{
            path: '/index/courses',
            name: '课程管理',
            component: () => import('@/views/courses/index')
        }, {
            path: '/index/checkAttendance',
            name: '考勤管理',
            component: () => import('@/views/checkAttendance/index')
        }, {
            path: '/index/analyse',
            name: '数据分析',
            component: () => import('@/views/analyse/index')
        }, {
            path: '/index/mine',
            name: '个人中心',
            component: () => import('@/views/mine/index')
        },
            {
                path: '/index/start',
                name: '开始使用',
                component: () => import('@/views/start/index')
            }

        ]
    }
];

const router = new VueRouter({
    routes
});

export default router;
