export function largeLineChartOption(checkLine, notCheckLine, xData) {
    return {
        title: {
            text: '今日出勤数据'
        },
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: ['出勤人数', '缺勤人数']
        },
        grid: {
            x: 35,
            y: 55,
            x2: 25,
            y2: 55

        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: xData
        },
        yAxis: {
            type: 'value',
            axisLabel: {
                formatter: '{value}人'
            }
        },
        dataZoom: [{
            'show': true,
            'height': 10,
            'xAxisIndex': [
                0
            ],
            bottom: '9%',
            'start': 0,
            'end': 100

        }, {
            type: 'inside',
            start: 0,
            end: 100
        }],
        series: [
            {
                name: '出勤人数',
                type: 'line',
                smooth: true,
                data: checkLine
            },
            {
                name: '缺勤人数',
                type: 'line',
                data: notCheckLine,
                smooth: true,
                lineStyle: {
                    color: 'red'
                },
                itemStyle: {
                    color: 'red'
                }
            }
        ]
    };
}

